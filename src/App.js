import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    value: "",
  };

  //handles the check box
  handleCheckBox = (id) => {
    let newTodos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      }
      return todo;
    });
    this.setState((state) => {
      return {
        ...state,
        todos: newTodos,
      };
    });
  };
  /////////////////////////////////////////////////////////////////////////




  // handles the Check Box Delete
  handleLineDelete = (event, id) => {
    const newTodoList = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({ todos: newTodoList });
  };

  handleDelAllLines = (event) => {
    const newTodoList = this.state.todos.filter((todo) => todo.completed === false);
    this.setState({ todos: newTodoList });
  };
  ////////////////////////////////////////////////////////////////////////////




  // Handles the input field
  handleInput = (event) => {
    if (event.key === "Enter") {
      // console.log("Enter")
      let newTodo = {
        userId: 1,
        id: Math.random() * 3000,
        title: event.target.value,
        completed: false,
      };
      let updateTodos = [...this.state.todos, newTodo];
      this.setState({ ...this.state, todos: updateTodos, value: "" });
    }
  };

  handleSubmitChange = (event) => {
    this.setState({ ...this.state, value: event.target.value });
  };
  ///////////////////////////////////////////////////////////////////////////////

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            onSubmit={this.handleSubmit}
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyDown={this.handleInput}
            onChange={this.handleSubmitChange}
            value={this.state.value}
          />
        </header>
        <TodoList
          todos={this.state.todos}
          handleCheckBox={this.handleCheckBox}
          handleLineDelete={this.handleLineDelete}
          handleDelAllLines={this.handleDelAllLines}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleDelAllLines}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  // Handles Completed Check Box

  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.handleCheckBox}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleLineDelete} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              key={todo.id}
              title={todo.title}
              completed={todo.completed}
              handleCheckBox={(event) => this.props.handleCheckBox(todo.id)}
              handleLineDelete={(event) => this.props.handleLineDelete(event, todo.id)
              }
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
